<?php
interface Color
{
    public function Paint();
}

class BlueColor implements Color
{
    public function Paint()
    {
       return ' blue color';
    }
}

class RedColor implements Color
{
    public function Paint()
    {
       return 'red color';
    }

}
abstract class Shape
{
    protected $color;

    public function __construct(Color $color)
    {
        $this->color = $color;
    }

   abstract public function draw();

}

class  Square extends Shape
{

    public function __construct(Color $color)
    {
        parent::__construct($color);
    }

    public function draw()
    {
        echo '<br>draw square with color:'.$this->color->Paint();
    }

}

class  Circle extends Shape
{

    public function __construct(Color $color)
    {
        parent::__construct($color);
    }

    public function draw()
    {
        echo '<br>draw circle with color:'.$this->color->Paint();
    }
}

class ClientDraw
{
    public static function draw()
    {
        $drawCircleBlue=new Circle(new BlueColor());
        $drawCircleBlue->draw();
        $drawCircleRed=new Circle(new RedColor());
        $drawCircleRed->draw();

        $drawSquareBlue=new Square(new BlueColor());
        $drawSquareBlue->draw();
        $drawSquareRed=new Square(new RedColor());
        $drawSquareRed->draw();
    }
}
ClientDraw::draw();

