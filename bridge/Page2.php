<?php

interface Render
{
    public function renderTitle(string $title):string ;
    public function renderTextBox(string  $text):string ;
    public function renderAll(array  $data):string ;
}

class HtmlRender implements Render
{
    public function renderTitle(string $title): string
    {
        return '<h4>'.$title.'</h4>';
    }

    public function renderTextBox(string $text): string
    {
        return '<p>'.$text.'</p>';
    }

    public function renderAll(array  $data): string
    {
      return  implode('',$data);
    }


}
class JsonRender implements Render
{
    public function renderTitle(string $title): string
    {
        return 'title:'.$title;
    }

    public function renderTextBox(string $text): string
    {
        return 'text:'.$text;
    }

    public function renderAll(array  $data): string
    {
        return  '{'.implode(',',$data).'}';
    }

}

abstract class Page
{
    protected Render  $render;

    public function __construct(Render $render)
    {
        $this->render = $render;
    }

    public function changeRender(Render $render):void
    {
        $this->render=$render;
    }

     abstract public function display();
}
class SimplePage extends Page
{

    protected string $title;
    protected string $text;
    public function __construct(Render $render,string $title, string $text)
    {
        parent::__construct($render);
        $this->title=$title;
        $this->text=$text;
    }

    public function display():string
    {
       return $this->render->renderAll(
            [
                $this->render->renderTitle($this->title),
                $this->render->renderTextBox($this->text)
            ]
        );
    }
}


class Product
{
    private string $id;
    private string $name;
    private string $description;


    public function
    __construct(
        string $id,
        string $name,
        string $description
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public function getId(): string
    {
        return $this->id;
    }


    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }


    public function setDescription(string $description): void
    {
        $this->description = $description;
    }


}


class ProductPage extends Page
{
    private Product  $product;

    public function __construct(Render $render,Product $product)
    {
        parent::__construct($render);
        $this->product=$product;
    }

    public function display():string
    {
        return $this->render->renderAll([
           $this->render->renderTitle($this->product->getName()),
            $this->render->renderTextBox($this->product->getDescription()),
        ]);
    }


}

$htmlRender=new HtmlRender();
$jsonRender=new JsonRender();


function clientRequest(Page $page){
    echo $page->display();
}


$simplePage=new SimplePage($htmlRender,'xin chao','cac ban');
clientRequest($simplePage);
$simplePage->changeRender($jsonRender);
clientRequest($simplePage);
//
//$product=new Product('123','sp1','san pham 1');
//$productPage=new ProductPage($htmlRender,$product);
//clientRequest($productPage);