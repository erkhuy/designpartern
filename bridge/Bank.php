<?php
interface Account
{
    public function actionType():void ;

}

class CheckingAccount implements Account
{
    public function actionType():void
    {
            echo 'Checking account';
    }

}

class SavingAccount implements Account
{
    public function actionType():void
    {
            echo 'saving account';
    }

}

class UpdatingAccount implements Account
{
    public function actionType():void
    {
        echo 'updating account';
    }

}

class OpeningAccount implements Account
{
    public function actionType():void
    {
        echo 'opening account';
    }

}



abstract class Bank
{
    protected  Account  $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

     abstract public function createWithType();
    public function changeAccountType(Account $action)
    {
        $this->account=$action;
    }

}

class AChauBank extends Bank
{
    public function __construct(Account $account)
    {
        parent::__construct($account);
    }

    public function createWithType():void
    {
        echo ' ACB Bank:';
        $this->account->actionType();
    }


}

class AgriBank extends Bank
{
    public function __construct(Account $account)
    {
        parent::__construct($account);
    }

    public function createWithType():void
    {
        echo 'AgriBank:';
        $this->account->actionType();
    }


}

class VietcomBank extends Bank
{
    public function __construct(Account $account)
    {
        parent::__construct($account);
    }

    public function createWithType():void
    {
        echo 'VietcomBank:';
        $this->account->actionType();
    }


}
class ViettinBank extends Bank
{
    public function __construct(Account $account)
    {
        parent::__construct($account);
    }

    public function createWithType():void
    {
        echo 'ViettinBank:';
        $this->account->actionType();
    }
}

class Clients
{
    protected string $name;
    protected Bank $bank;

    public function __construct(string $name, Bank $bank)
    {
        $this->name = $name;
        $this->bank = $bank;
    }

    public function changeBank(Bank $bank)
    {
        $this->bank=$bank;
    }

    public function changeAction(Account $action)
    {
        $this->bank->changeAccountType($action);
    }

    public  function createAccount()
    {
        echo 'user:'.$this->name;
        echo 'use '.$this->bank->createWithType();
    }
}

$client=new clients('huy',new VietcomBank(new OpeningAccount()));
$client->createAccount();
$client->changeAction(new SavingAccount());
$client->createAccount();
