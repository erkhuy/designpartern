<?php

interface VietNameseTarget
{
    public function sent(string $msg);
}

class EnglishAdaptee
{
    public function recevice(string $word)
    {
        echo 'recevice form Adapter';
        echo $word;
    }

}

class TranslatorEnglishAdapter implements VietNameseTarget
{
    private $englishAdaptee;

    public function __construct(EnglishAdaptee $englishAdaptee)
    {
        $this->englishAdaptee = $englishAdaptee;
    }

    public function sent(string $word)
    {
        echo 'read' . $word;
        $this->englishAdaptee->recevice($this->translate($word));
    }

    public function translate(string $word)
    {
        return $word . 'is translate';
    }
}

class VietnameseClient
{
    private $client;
    private $msg;

    public function __construct(VietNameseTarget $client, $msg)
    {
        $this->client = $client;
        $this->msg = $msg;
    }

    public function sent($msg)
    {
        $this->client->sent($msg);
    }

    public function getMsg()
    {
        return $this->msg;
    }
}

$vietnameseClient=new VietnameseClient(new TranslatorEnglishAdapter(new EnglishAdaptee()),'xin chao');
$vietnameseClient->sent($vietnameseClient->getMsg());