<?php

namespace Php\Adapter\SentMessage;

interface Notification
{
    public function send(string $title, string $message);
}

class EmailNotification implements Notification
{
    private  $adminEmail;

    public function __construct(string $adminEmail)
    {
        $this->adminEmail = $adminEmail;
    }

    public function send(string $title, string $message): void
    {
        echo "Gửi mail với Tiêu đề  '$title' Đến '{$this->adminEmail}' Tin nhắn '$message'.";
    }
}

class SlackApi
{
    private  $login;
    private  $apiKey;

    public function __construct(string $login, string $apiKey)
    {
        $this->login = $login;
        $this->apiKey = $apiKey;
    }

    public function logIn(): void
    {
        echo "Đăng nhập với '{$this->login}'.\n";
    }

    public function sendMessage(string $chatId, string $message): void
    {
        // Send message post request to Slack web service.
        echo "gứi tin nhẵn đến '$chatId' chat: '$message'.\n";
    }
}

class SlackNotification implements Notification
{
    private  $slack;
    private  $chatId;

    public function __construct(SlackApi $slack, string $chatId)
    {
        $this->slack = $slack;
        $this->chatId = $chatId;
    }

    public function send(string $title, string $message): void
    {
        $slackMessage = "#" . $title . "# " . strip_tags($message);
        $this->slack->logIn();
        $this->slack->sendMessage($this->chatId, $slackMessage);
    }
}

function clientCode(Notification $notification)
{
    echo $notification->send('website lỗi!',
        "<strong style='color:#fd62ff;font-size: 50px;'>Alert!</strong> " .
        'Website đang bị lỗi GỌi admin Để sửa lỗi!');

}

echo '</br>Client code is designed correctly and works with email notifications:</br>';
$notification = new EmailNotification('developers@example.com');
clientCode($notification);
echo "\n\n";


echo '</br>Sử dụng với bộ chuyển đổi:</br>';
$slackApi = new SlackApi('example.com', 'XXXXXXXX');
$notification = new SlackNotification($slackApi, 'Example.com Developers');
clientCode($notification);