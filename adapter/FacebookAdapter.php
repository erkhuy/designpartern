<?php


class Facebook
{
    private  $msg;

    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    public function getMsg()
    {
        return $this->msg;
    }
    public function postMessage($msg)
    {
        echo '</br> facebook message '.$msg.'</br>';
    }
}

interface SocialAdapter
{
    public function postMessage($message);
}

class FacebookAdapter implements SocialAdapter
{
    protected $facebook;

    public function __construct(Facebook $facebook)
    {
        $this->facebook=$facebook;
    }

    public function postMessage($message)
    {
        $this->facebook->postMessage($message);
    }

}

$msg='hello word';
$fb=new Facebook($msg);

$facebook=new FacebookAdapter($fb);
$facebook->postMessage($fb->getMsg());
