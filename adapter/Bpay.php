<?php
require ('Xpay.php');

class Bpay implements Xpay
{


    private $creditCardNo;
    private $customerName;
    private $amount;

    public function getCreditCardNo()
    {
        return $this->creditCardNo;
    }

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setCreditCardNo($creditCardNo)
    {
        $this->creditCardNo=$creditCardNo;
    }

    public function setCustomerName($customerName)
    {
        $this->customerName=$customerName;
    }

    public function setAmount($amount)
    {
        $this->amount=$amount;
    }
    public function __toString()
    {
        return 'CardNo:'.$this->creditCardNo.'customerName:'.$this->customerName.' amount:'.$this->amount;
    }

}