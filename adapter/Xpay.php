<?php


interface Xpay{
    public function getCreditCardNo();
    public function getCustomerName();
    public function getAmount();

    public function setCreditCardNo($creditCardNo);
    public function setCustomerName($customerName);
    public function setAmount($amount);
}


interface PayD{
    public function getCreditCardNo();
    public function getCustomerName();
    public function getAmount();

    public function setCreditCardNo($creditCardNo);
    public function setCustomerName($customerName);
    public function setAmount($amount);
}