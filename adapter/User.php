<?php

interface UserInterface
{
    public function setName($name);
    public function getName();
}

class User implements UserInterface
{
    private $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}


interface CustomerInterface
{
    public function setFirstName($fname);
    public function getFirstName();
    public function setLastName($lname);
    public function getLastName();
}
class UserToCustomerAdapter implements CustomerInterface
{
    protected User $user;

    protected string $firstName;
    protected string $lastName;
    public function __construct(User $user)
    {
        $this->user = $user;
        // Lấy fullname từ object cần chuyển đổi (User)
        $fullname = trim($this->user->getName());
        //Tách chuỗi
        $pieces = explode(" ", $fullname);
        $this->firstName = $pieces[0];
        $this->lastName = $pieces[count($pieces)-1];
    }

    public function setFirstName($fname):void
    {
        $this->firstName = $fname;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lname):void
    {
        $this->lastName = $lname;
    }

    public function getLastName()
    {
        return $this->lastName;
    }
}


$user = new User;
$user->setName(" Ha quang huy ");



$adapter = new UserToCustomerAdapter($user);
//Sử dụng

$firstName = $adapter->getFirstName();
$lastName = $adapter->getLastName();
echo "Customer's first name: {$firstName}, last name: {$lastName}";
